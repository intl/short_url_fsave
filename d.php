<?php
function base64url_decode($data) 
{
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}
if(strlen($_SERVER['QUERY_STRING'])>0)
    {
    echo base64url_decode($_SERVER['QUERY_STRING']);
    }
else
    {
    header('Location: /index.html');
    }
?>